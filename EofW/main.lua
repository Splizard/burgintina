local loader = require "AdvTiledLoader/Loader"
-- set the path to the Tiled map files
loader.path = "maps/"

require "camera"

local heroes = require "heroes"

local graphics = love.graphics


function love.load()

    -- load the level and bind to variable map
    map = loader.load("map.tmx")

    heroes.newHero("uni", {anim="data/anim/uni.anim",image=graphics.newImage("data/uni.png")})

    -- set up the hero object.
    player = heroes.init(map, collider)

end

function love.update(dt)

    --Update heroes
    heroes.update(dt)
    
    local x , y = player.x, player.y
    
    camera:setPosition((x*128)-300+128, (y*128)-400+128)
    map:setDrawRange(camera.x,camera.y,love.graphics.getWidth(), love.graphics.getHeight())

end

function love.draw()
    camera:set()
    
    --Draw Map.
    map:draw()
    
    camera:unset()
	love.graphics.setColor(255,255,255)
	love.graphics.print("FPS: "..love.timer.getFPS().."Pos:"..player.x.." "..player.y,0, 16)
end

