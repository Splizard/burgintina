animation = {}
animation.__entities = {}
animation.__animations = {}
animation.__entitiesByID = {}
animation.__keyframes = 1
animation.__speed = 0.2
animation.__images = {}
animation.__images.__call = function(self, image, path)
	if self[image] then
		return self[image]
	else
		self[image]=love.graphics.newImage(path..image)
		return self[image]
	end
end
setmetatable(animation.__images, animation.__images)


local __entity_I = 0
local __animation_I = 1
local __keyframe = 1
local file

local play = true

--Render animations.
animation.render = function(lg)
	for i,anim in pairs(animation.__animations) do
		anim:draw(lg)
	end
	animation.draw()
	local bone, key, anim, layer, mode
	if __entity_I > 0 then bone = animation.__entities[__entity_I].name else bone = "nil" end
	if __entity_I > 0 then layer = animation.__entities[__entity_I].layer or 0 else layer = 0 end
	if __animation_I > 0 then anim = animation.__animations[__animation_I].name else anim = "nil" end
	if __animation_I > 0 then key = animation.__animations[__animation_I].keyframe else key = "nil" end
	if __animation_I > 0 then mode = animation.__animations[__animation_I].mode or "nil" else mode = "nil" end
	lg.print("FPS: "..love.timer.getFPS().." BONE:"..bone,0, 16*1)
	lg.print("WASD moves body, ~ switches animation, TAB switches bone, X saves, Y creates new animation, arrow keys move selected bone.",0, 16*2)
	lg.print("[] change layers, +/- switch keyframe, M change mode, K create keyframe, R remove last keyframe, spacebar play/pause.",0, 16*3)
	lg.print("Animation: "..anim.." Keyframe: "..key.." Layer: "..layer.." Mode: "..mode,0, 16*4)
	lg.rectangle("fill",400,300,128,128)
end

animation.__flip = false

--Handle keypresses.
animation.keypressed = function(key, unicode)
	local anim = animation.__animations[__animation_I]
	if key == "lshift" then
		__entity_I = __entity_I - 1
		if __entity_I < 0 then
			__entity_I = #animation.__entities
		end
	end
	if key == "tab" then
		__entity_I = __entity_I + 1
		if __entity_I > #animation.__entities then
			__entity_I = 1
		end
	end
	if key == "`" then
		__animation_I = __animation_I + 1
		if __animation_I > #animation.__animations then
			__animation_I = 0
		end
	end
	if key == "f" then
		if __entity_I > 0 then
			local self = animation.__entities[__entity_I]
			self.sx = -self.sx
		end
	end
	if key == "m" then
		if anim.mode == "loop" then
			anim.mode = "once"
		elseif anim.mode == "once" then
			anim.mode = "loop"
		end
		if anim.mode == nil then
			anim.mode = "loop"
		end
	end
	if key == "t" then
		flip = not flip
	end
	if key == "x" then
		local file = io.open("uni.anim", "w")
		anim:save(file)
		file:close()
		print("saved")
	end
	if key == "y" then
		if __entity_I > 0 then
			animation.newAnimation(animation.__entities[__entity_I])
			__animation_I = 1
		end
	end
	if key == "=" then
			anim.keyframe =anim.keyframe + 1
			if anim.keyframe > anim.keyframes then
				anim.keyframe = 1
			end
	end
	if key == "k" then
			anim.keyframes = anim.keyframes + 1
			anim.keyframe = anim.keyframes
	end
	if key == "r" then
			anim.keyframes = anim.keyframes - 1
			for id,enty in pairs(animation.__entities) do
				enty:removeKeyframe(anim.keyframes+1)
			end
	end
	if key == "-" then
			anim.keyframe = anim.keyframe - 1
			if anim.keyframe <= 0 then
				anim.keyframe = anim.keyframes
			end
	end
	if key == "[" then
		if __entity_I > 0 then
			local self = animation.__entities[__entity_I]
			self.layer = (self.layer or 0)-1
			if self.layer < -10 then
				self.layer = -10
			end
		end
	end
	if key == "]" then
		if __entity_I > 0 then
			local self = animation.__entities[__entity_I]
			self.layer = (self.layer or 0)+1
			if self.layer > 10 then
				self.layer = 10
			end
		end
	end
	if key == " " then
		if anim.playing then
			anim:stop()
		else
			anim:play()
		end
	end
end

--Handle mousepresses.
animation.mousepressed = function(x, y, button)
	if button == "l" then
		if __entity_I > 0 then
			local self = animation.__entities[__entity_I]
			self.ofx = x-self.rx+self.ox
			self.ofy = y-self.ry+self.oy
			print(self.ofx,self.ofy)
		end
	end
end

--Edit animations in edit mode.
animation.edit = function()
	local anim = animation.__animations[__animation_I]
	if (not anim.playing) and __entity_I > 0 then
		local k = anim.keyframe
		local sprite = animation.__entities[__entity_I]
		local self = sprite
		if love.keyboard.isDown("up") then
			sprite.oy[k] = (sprite.oy[k] or sprite:get("oy",k)) + 1

		elseif love.keyboard.isDown("down") then
			sprite.oy[k] = (sprite.oy[k] or sprite:get("oy",k)) - 1

		end
		if love.keyboard.isDown("left") then
			sprite.ox[k] = (sprite.ox[k] or sprite:get("ox",k)) + 1

		elseif love.keyboard.isDown("right") then

			sprite.ox[k] = (sprite.ox[k] or sprite:get("ox",k)) - 1
		end
		if love.keyboard.isDown(",") then
			sprite.r[k] = (sprite.r[k] or sprite:get("r",k)) + 0.02
			--local vx = self.ofx*math.cos(-sprite.r) + self.ofy*math.sin(-sprite.r)
			--local vy = self.ofy*math.cos(-sprite.r) - self.ofx*math.sin(-sprite.r)
			--sprite.ox2 = sprite.ox-vx
			--sprite.oy2 = sprite.oy-vy
		elseif love.keyboard.isDown(".") then
			sprite.r[k] = (sprite.r[k] or sprite:get("r",k)) - 0.02
			--local vx = self.ofx*math.cos(-sprite.r) + self.ofy*math.sin(-sprite.r)
			--local vy = self.ofy*math.cos(-sprite.r) - self.ofx*math.sin(-sprite.r)
			--sprite.ox2 = sprite.ox-vx
			--sprite.oy2 = sprite.oy-vy
		end
	end
end

animation.Animation = {}
animation.Animation.__index = animation.Animation
animation.newAnimation = function(name)
	local anim = {}
	setmetatable(anim, animation.Animation)
	anim.name = tostring(name)
	anim.speed = 1
	anim.keyframe = 1
	anim.keyframes = 1
	anim.lastTime = 0
	anim.timer = 0
	anim.x = 0
	anim.y = 0
	anim.r = 0
	anim.mode = "loop"
	anim.playing = false
	anim.entities = {}
	anim.imagePath = "data/"
	table.insert(animation.__animations,anim)
	return anim
end

--Return entity from animation.
function animation.Animation:getEntity(name)
	for i,v in pairs(self.entities) do
		if v.name == name then
			return v
		end
	end
end

function animation.Animation:setImagePath(path)
	anim.imagePath = path
end

--Create enitities for animation.
function animation.Animation:newEntities(list)
	for i,v in pairs(list) do
		animation.newEntity(v[1],v[2], #animation.__animations)
		
	end
end

--Draw animation
function animation.Animation:draw(lg, x, y, r)
	for i,enty in pairs(self.entities) do
		if not enty.child then
			enty:draw(lg, self.x+(x or 0), self.y+(y or 0), self.r+(r or 0))
		end
	end
end


--An entity is a part of the animation.
animation.Entity = {}
animation.Entity.__index = animation.Entity
animation.newEntity = function(name, list, animationID)
	local enty = {}
	setmetatable(enty, animation.Entity)
	enty.name = tostring(name)
	enty.x = 0
	enty.y = 0
	enty.ox = {[1]=0}
	enty.oy = {[1]=0}
	enty.rx = 0
	enty.ry = 0
	enty.r = {[1]=0}
	enty.sx = 1
	enty.sy = 1
	enty.speed = animation.__speed
	enty.timer = 0
	enty.playing = false
	enty.lastTime = love.timer.getTime( )
	enty.ofx = enty.image and enty.image:getWidth()/2
	enty.ofy = enty.image and enty.image:getHeight()/2
	enty.child = false
	enty.children = {}
	if animationID then
		table.insert(animation.__animations[animationID].entities,enty)
		enty.anim = animation.__animations[animationID]
	end
	table.insert(animation.__entities,enty)
	animation.__entitiesByID[name] = enty
	
	--print(name, list)
	
	--Loop through and create children.
	if list then
		--Single child.
		if type(list[1]) == "string" then
			 enty:addChild(animation.newEntity(list[1],list[2], animationID))
		else
			--Multi child.
			for i,v in pairs(list) do
				if type(v[1]) == "string" then
					enty:addChild(animation.newEntity(v[1], v[2], animationID))
				end
			end
		end
	end
	
	return enty
end

--Remove specific keyframe from entity.
function animation.Entity:removeKeyframe(keyframe)
	self.ox[keyframe] = nil
	self.oy[keyframe] = nil
	self.r[keyframe] = nil
end

--Return keyframe item "key" from value name.
--If it does not exist return the previous value.
function animation.Entity:get(name, key)
	if self[name][key] then return self[name][key] end
	if key > self.anim.keyframes then
		return self[name][1] or 0
	end
	for i=key, 1, -1 do
		if self[name][i] then return self[name][i] end
		
	end
	return 0
end


--Loading animations from a file.
local loadChildFor = nil

function animation.loadChildFor(name)
	loadChildFor = animation.__entitiesByID[name]
end

function animation.loadEntity(name, image, tble)
	local enty = tble
	setmetatable(enty, animation.Entity)
	enty.imagepath = image
	enty.image = love.graphics.newImage(image)
	enty.child = false
	enty.children = {}
	enty.speed = enty.speed or animation.__speed
	enty.nspeed = enty.speed
	enty.anim = {}
	enty.timer = 0
	enty.lastTime = love.timer.getTime( )
	table.insert(animation.__entities,enty)
	animation.__entitiesByID[name] = enty
	if loadChildFor then
		loadChildFor:addChild(enty)
		loadChildFor = nil
	end
end

function animation.loadAnimation(list, path)
	--Load animations.
	local anim = list.anim
	setmetatable(anim, animation.Animation)
	anim.lastTime = 0
	anim.playing = false
	anim.imagePath = path
	anim.entities = {}
	table.insert(animation.__animations,anim)
	
	anim:newEntities(list.entities)
	
	--Load keyframes.
	for i,v in pairs(list.keyframes) do
		enty = anim:getEntity(i)
		enty.ox = v["ox"]
		enty.oy = v["oy"]
		enty.r = v["r"]
		enty.layer = v["layer"] or 0
	end
	
	--Load images.
	
	for i,v in pairs(list.images) do
		enty = anim:getEntity(i)
		enty:setImage(v[1])
	end
	
	return anim
end

--Save entity block.
function animation.Entity:save(file,tabs)
	local spacing = {}
	for i=1, tabs do
		spacing[i] = "\t"
	end
	spacing = table.concat(spacing)

	file:write(spacing.."{'"..self.name.."'")
	if #self.children > 1 then
		file:write(",{\n")
		for i, enty in pairs(self.children) do
			enty:save(file, tabs+1)
			if i < #self.children then
				file:write(",\n")
			end
		end
		file:write("}}")
	elseif #self.children > 0 then
		file:write(",\n")
		self.children[1]:save(file, tabs+1)
		file:write("}")
	else
		file:write("}")
	end 
end


local function seri(name, table,file) 
	if not table then table = {} end
	if #table > 0 then
		file:write("\t\t\t"..name.." = {\n")
		for i,v in pairs(table) do
			file:write("\t\t\t\t["..i.."]="..v..",\n")
		end
		file:write("\t\t\t},\n")
	end
end

function animation.Animation:save(file)
	--Save animation.
	file:write("local animation = {\n\tanim = {\n")
	for i,v in pairs(self) do
		if type(v) == "number" then
			file:write("\t\t"..i.."="..v..",\n")
		elseif type(v) == "string" then
			file:write("\t\t"..i.."='"..v.."',\n")
		end
	end
	--Save heirachy.
	file:write("\t},\n\tentities = {\n")
	for i,enty in pairs(self.entities) do
		if not enty.child then
			enty:save(file,2)
		end
	end
	--Save keyframes.
	file:write("\n\t},\n\tkeyframes = {\n")
	for i,enty in pairs(self.entities) do
		file:write("\n\t\t['"..enty.name.."'] = {\n")
		seri("ox",enty.ox, file)
		seri("oy",enty.oy, file)
		seri("r",enty.r, file)
		if enty.layer ~= 0 then
			file:write("\t\t\tlayer = "..enty.layer..",\n")
		end
		file:write("\t\t},\n")
	end
	file:write("\t},\n\timages = {\n")
		for i,enty in pairs(self.entities) do
			file:write("\n\t\t['"..enty.name.."'] = {")
			file:write("'"..enty.imagePath.."'")
			file:write("},")
		end
	file:write("\n\t}\n}\nreturn animation")
	--[[
	seri("ox",self.ox, file)
	seri("oy",self.oy, file)
	seri("r",self.r, file)
	file:write("})")
	for i,child in pairs(self.children) do
		file:write("\nanimation.loadChildFor('"..self.name.."')")
		child:save(file)
	end]]
end

--Set image of entity.
function animation.Entity:setImage(image)
	self.image = animation.__images(image,self.anim.imagePath)
	--self.ox = {[1]=self.image and self.image:getWidth()/2}
	--self.oy = {[1]=self.image and self.image:getHeight()/2}
	self.ofx = self.image and self.image:getWidth()/2
	self.ofy = self.image and self.image:getHeight()/2
	self.imagePath = image
end

function animation.Animation:play()
	self.playing = true
	self.lastTime = love.timer.getTime( )
	self.timer = 0
	if self.playing and self.mode == "once" then
		self.keyframe = 1
	end
end

function animation.Animation:setSpeed(n)
	self.nspeed = n
end

function animation.Animation:stop()
	self.playing = false
end



function animation.Entity:run()
	self.playing = true
end

function animation.Entity:stop()
	self.playing = false
end

function animation.Entity:addChild(enty)
	enty.child = true
	table.insert(self.children, enty)
end

local function tween(v0, v1, t)
  return v0+((v1-v0)*t)
end

local __timer = 0

--Manage animation timing.
animation.play = function(dt)
	for i,anim in pairs(animation.__animations) do
		if anim.playing then
			anim.timer = anim.timer + dt
			if anim.timer > (anim.nspeed or anim.speed) then
				anim.timer = 0
				anim.lastTime = love.timer.getTime( )
				anim.keyframe = anim.keyframe + 1
				if anim.keyframe >= anim.keyframes+1 then
					anim.keyframe = 1
				end
			elseif anim.keyframe > anim.keyframes then
				anim.keyframe = 1
			end
			if anim.keyframe == anim.keyframes then
				if anim.mode == "once" then
					anim.playing = false
				end
			end
		end
	end
end

lastTime = love.timer.getTime( )

local __toDraw = {} 

function animation.addDraw(layer, ...)
	if not __toDraw[layer] then __toDraw[layer] = {} end
	table.insert(__toDraw[layer],{...})
end

--Drawing
function animation.draw()
	--Manage draw queue.
	for i=-10, 10 do --Go through layers.
		if __toDraw[i] then	
			for i, v in pairs(__toDraw[i]) do	
				if animation.__flip then
					love.graphics.translate(v[2]*2,0)
					love.graphics.scale(-1,1)
				end
				love.graphics.draw(v[1],v[2],v[3],v[4],v[5],v[6],v[7],v[8],v[9])
				if animation.__flip then
					love.graphics.scale(-1,1)
					love.graphics.translate(-(v[2]*2),0)
				end
			end
			__toDraw[i] = {}
		end
	end
end

function animation.Entity:draw(lg, x, y, r, ox ,oy, scale)
	x, y, r, ox, oy = x or 0, y or 0, r or 0, ox or 0, oy or 0
	if self.image then
		local k = self.anim.keyframe
		local scale
		if self.anim.playing then scale = (love.timer.getTime( )-self.anim.lastTime)/(self.anim.nspeed or self.anim.speed) end
		--Play values.
		local ox_play = (self.anim.playing and tween(self:get("ox",k),self:get("ox",k+1),scale)) or self:get("ox",k)
		local oy_play = (self.anim.playing and tween(self:get("oy",k),self:get("oy",k+1),scale)) or self:get("oy",k) 
		local r_play = (self.anim.playing and tween(self:get("r",k),self:get("r",k+1),scale)) or self:get("r",k)
		
		--Add to a draw queue (layers support).
		animation.addDraw(self.layer or 0,self.image,self.x+x, self.y+y, r_play+r, self.sx, self.sy, ox_play, oy_play)
		
		self.rx = self.x+x
		self.ry = self.y+y
		
		if not play then
			lg.setColor(255,255,255)
			lg.point(x+self.x- ox_play+self.ofx, y+self.y-oy_play+self.ofy)
		end
		if #self.children > 0 then
			for i,child in pairs(self.children) do
			
				child:draw(lg, x+self.x, y+self.y, r+r_play,self.rx,self.ry, scale)
			end
		end
	end
end

local loadRAW = function(file)
	return dofile(file)
end

function animation.load(filename, path)
	local file = io.open(filename,"r")
	if file then
		file:close()
		
		status, anim = pcall(loadRAW, filename)
		if not status then io.write("[ERROR] "..anim.."\n") return false end	
		
		return 	animation.loadAnimation(anim, path)
	else
		io.write("[ERROR] File: "..filename.." not found!\n") 
		return false
	end
end


