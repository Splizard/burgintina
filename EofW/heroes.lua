local heroes = {}
heroes.__heroes = {}
heroes.__heroesInGame = {}
heroes.__collider = nil
local HC = require "HardonCollider"

local world

local sliding_speed = 1000
local gravity = 100

require ("anim")

heroes.Hero = {}
heroes.Hero.__index = heroes.Hero

--Register new Hero.
heroes.newHero = function(id, table)
	local hero = {}
	hero.name = tostring(table.name)
	hero.image = table.image
	hero.anim = table.anim
    hero.speed = 500
	heroes.__heroes[id] = hero
end

--Spawn Hero in the game.
heroes.spawnHero = function(id, x, y)
	local hero = {}
	setmetatable(hero, heroes.Hero)
	hero.name = tostring(name)
	hero.image = heroes.__heroes[id].image
	hero.x = tonumber(x)
	hero.y = tonumber(y)
	hero.vx = 0
	hero.vy = 0
	hero.width = hero.image:getWidth()/128
	hero.speed = heroes.__heroes[id].speed
	hero.jumpSpeed = 240
	hero.jump = 50
	hero.sprint = 0
	table.insert(heroes.__heroesInGame,hero)
	
	if heroes.__heroes[id].anim then 
		hero.anim = {}
		hero.anim["stand"] = animation.load("data/anim/uni_stand.anim", "data/")
		hero.anim["stand"]:play()
		hero.anim["walk"] = animation.load("data/anim/uni_walk.anim", "data/")
		hero.anim["walk"]:play()
		hero.anim["jump"] = animation.load("data/anim/uni_jump.anim", "data/")
		hero.anim["hang"] = animation.load("data/anim/uni_hang.anim", "data/")
		hero.anim["hangup"] = animation.load("data/anim/uni_hangup.anim", "data/")
	end
	
	return hero
end

function round(num, idp)
  local mult = 10^(idp or 0)
  return math.floor(num * mult + 0.5) / mult
end

function heroes.Hero:getTile(x,y)
	x, y = x or 0, y or 0
	return map.layers["ground"]:get(round(self.x+x), round(self.y+y))
end

function tile_dos(tile)
	if tile then
		if tile.properties.diagonal and tile.properties.slide then
			return "slide"
		else
			return tile.properties.diagonal
		end
		return false
	end
	return false
end

function tile_s(tile)
	if tile then
		if tile.properties.slide then
			return true
		end
		return false
	end
	return false
end

function tile_d(tile)
	if tile then
		if tile.properties.diagonal then
			return tile.properties.diagonal
		end
		return false
	end
	return false
end

function tile_nd(tile)
	if tile then
		if tile.properties.diagonal then
			return false
		end
		return true
	end
	return false
end

function slope_check(tile,x,y,yy)
	yy = yy or 0
	if tile and tile.properties.diagonal == "right" then
		--Complicated Math to detect collsion on slope.
		--DO NOT CHANGE UNLESS YOU REALLY KNOW WHAT YOUR DOING.
		if y+(yy or 0) > ((math.floor(y))+(x-round(x-1)-0.4)) then
			return math.floor(y)+(x-round(x-1)-0.4)
		end
	elseif tile and tile.properties.diagonal == "left" then
		--Complicated Math to detect collsion on slope.
		--DO NOT CHANGE UNLESS YOU REALLY KNOW WHAT YOUR DOING.
		if y+(yy or 0) > ((math.floor(y))+(1-(x-round(x-1)-0.4))) then
			return (math.floor(y))+(1-(x-round(x-1)-0.4))
		end
	end
end

--Move and collision detection.
function heroes.Hero:move(dt, x,y)
	x,y = x/1000, y/1000
	--print(math.floor(self.x), math.floor(self.y))
	--print(self.x, self.y)
	
	local area = map.layers["ground"]
	
	--print(self.x,self.y)
	
	local bottom = self:getTile(0, y+0.4)
	
	local w, wi
	if y ~= 0 then
		if self.facing == "left" then w = -0.5 else w = 0.5 end
		
		--Sliding/Slopes/Stairs Collision
		local top = self:getTile(0,y-1.3)
		local topleft = self:getTile(-0.2,y-1.3)
		local topright = self:getTile(0.2,y-1.3)
		local acrossleft = self:getTile(-0.2, y+0.5)
		local acrossright = self:getTile(0.2, y+0.5)
		local bottomleft = self:getTile(-0.2, y-0.5)
		local bottomright = self:getTile(0.2, y-0.5)	
		
		
		if self.jumping then
			if self:getTile(0, y-1.5) then
				self.jump = self.jump - 20
			end
		end
		
		
		if tile_d(bottom) or tile_d(acrossleft) or tile_d(acrossright) then
			bottom_slope = slope_check(bottom,self.x,self.y, y)
			
			if bottom_slope then
				if bottom.properties.slide then
					self.sliding = bottom.properties.diagonal
				end
				self.y = bottom_slope
				self.ground = true
				self.slope = true
			
			--This stops bobbing up and down.
			elseif (not self.jumping) and ( (bottomleft and bottomleft.properties.diagonal) and (acrossright and acrossright.properties.diagonal)) then
				--if self.sprint == 0 then
					--if self.y + y > math.floor(self.y)+ (self.x-round(self.x)-0.4) then
						--self.y = math.floor(self.y)+ (self.x-round(self.x)-0.4)
					--end
				--end
				if tile_s(bottomleft) and tile_s(acrossright) then
					self.sliding = bottomleft.properties.diagonal
				end
			elseif (not self.jumping) and ( (bottomright and bottomright.properties.diagonal) and (acrossleft and acrossleft.properties.diagonal)) then
				--if self.sprint == 0 then
					--if self.y + y > math.floor(self.y)+ (1-(self.x-round(self.x)-0.4)) then
						--self.y = math.floor(self.y)+ (1-(self.x-round(self.x)-0.4))
					--end
				--end
				if tile_s(bottomright) and tile_s(acrossleft) then
					self.sliding = bottomright.properties.diagonal
				end
			--Ground/Ceilings Collsion
			--This needs to be here.
			elseif (bottom and (not bottom.properties.diagonal)) then
				self.y = round(self.y)
				self.ground = true
				self.sliding = false
				self.slope = false
				self.sliding = false
			else
				
				self.ground = false
				self.y = self.y + y
				self.slope = false
				--self.sliding = false
				
				--Smoothness
				local acrossleft = self:getTile(-0.2, y+0.5)
				local acrossright = self:getTile(0.2, y+0.5)
				local bottomleft = self:getTile(-0.2, y-0.5)
				local bottomright = self:getTile(0.2, y-0.5)	
				inside_slope = slope_check(self:getTile(0,0),self.x,self.y,y)
				if ( (bottomright and bottomright.properties.diagonal) and (acrossleft and acrossleft.properties.diagonal)) then
					if self.y + y > math.floor(self.y)+ (1-(self.x-round(self.x)-0.4)) then
						self.y = math.floor(self.y)+ (1-(self.x-round(self.x)-0.4))
					end
					if tile_s(bottomright) and tile_s(acrossleft) then
						self.sliding = bottomright.properties.diagonal
					end
				end
			end
			
		--Ground/Ceilings Collsion
		elseif bottom or tile_nd(acrossleft) or tile_nd(acrossright) or top or topleft or topright then
			self.y = round(self.y)
			self.ground = true
			self.sliding = false
			self.slope = false
		--No Collision
		else
			self.ground = false
			self.y = self.y + y
		end
	end
	
	if self.ground and (not self.moving) then
		if self.sliding == "right" then
			x = (5*sliding_speed*dt)/1000
		elseif self.sliding == "left" then
			x = -(5*sliding_speed*dt)/1000
		end
	end
	
	if x ~= 0 then
		if self.facing == "left" then w = -0.5 else w = 0.5 end
		if self.facing == "left" then wi = -0.4 else wi = 0.4 end
		
		if self.jumping then
			if area:get(round(self.x+x+wi),round(self.y-0.5)) and (not area:get(round(self.x+x+wi),round(self.y-0.5)).properties.diagonal) and not( area:get(round(self.x+x+wi),round(self.y-1.5))) 
			and not( area:get(round(self.x),round(self.y-2.5))) and not (area:get(round(self.x+x+wi),round(self.y-2.5))) then
			--print(round(self.x+x+w),round(self.y))
				if math.roughly_equal(self.y, round(self.y-0.5)+0.4, 0.05) then
					
					self.hanging = true
					self.x, self.y = round(self.x+x+wi)-(wi*2), round(self.y-0.5)+0.4
					self.ground = false
					self.anim["hang"]:play()
					self.jumping = false
				end
			end
		end
		
		local bottom = self:getTile(x, 0.4)
		local top = self:getTile(x,y-1.3)
		local topleft = self:getTile(-0.2+x,1.3)
		local topright = self:getTile(0.2+x,1.3)
		local acrossleft = self:getTile(-0.2+x, 0.5)
		local acrossright = self:getTile(0.2+x, 0.5)
		local bottomleft = self:getTile(-0.2+x, 0.5)
		
		--Sliding/Slopes/Stairs
		local across = self:getTile(x, 0)
		local down = self:getTile(0, 0.5)
		local acrossbottom = self:getTile(x+w, 0.5)
		local bottom = self:getTile(x+w, -0.5)
		local top = self:getTile(x+w, 1.5)
		local case = tile_d(across) or tile_d(acrossbottom) or tile_d(bottom) or tile_d(top) or tile_d(down)
		if (not self.sliding) and tile_s(across) then
			--Keep to grid
			self.x = round(self.x+x+w) - w
			--print("oi")
			self.moving = false
		elseif case then
				self.x = self.x + x
				bottom_slope = slope_check(bottom,self.x,self.y)
				if bottom_slope then
					self.y = bottom_slope
				elseif case and ( (x < 0 and case == "right") or (x > 0 and case == "left") ) then
					self.y = self.y - math.abs(x)
				else
					self.y = self.y + math.abs(x)
				end
				
				--[[if ((across and across.properties.diagonal == "right") or (bottom and bottom.properties.diagonal == "right")) and self.facing == "left" then
					--Complicated Math to detect collsion on slope.
					--DO NOT CHANGE UNLESS YOU REALLY KNOW WHAT YOUR DOING.
					if self.y > ((math.floor(self.y))+(self.x-round(self.x)-0.4)) then
						self.y = (math.floor(self.y))+(self.x-round(self.x)-0.4)
					else
						self.y = self.y -0.1
					end
				elseif ((across and across.properties.diagonal == "left") or (bottom and bottom.properties.diagonal == "left")) and self.facing == "right" then
					--Complicated Math to detect collsion on slope.
					--DO NOT CHANGE UNLESS YOU REALLY KNOW WHAT YOUR DOING.
					if self.y > (math.floor(self.y))+(1-(self.x-round(self.x)-0.4)) then
						self.y = (math.floor(self.y))+(1-(self.x-round(self.x)-0.4))
					else
						self.y = self.y -0.1
					end
				end]]
				--self.y = math.floor(self.y)+self.x-round(self.x-1)-0.5+0.01
		elseif area:get(round(self.x+x+wi),round(self.y)) or area:get(round(self.x+x+wi),round(self.y-1)) or area:get(round(self.x+x+wi),round(self.y-1.3)) then
			--Keep to grid
			self.x = round(self.x+x+wi) - wi*2
			--print("oi")
			self.moving = false
		else
			self.x = self.x + x
		end
		--print(map.layers["ground"]:get(math.floor(self.x-1),math.floor(self.y+0.5)))
	end
end

function math.roughly_equal(x,y,diff)
	if x > (y-diff) and x < (y+diff) then
		return true
	end
	return false
end

--Update heroes.
heroes.update = function(dt)
	for i,hero in pairs(heroes.__heroesInGame) do
		--print(hero.x, hero.y)
		hero:move(dt, 0, gravity, true)
		
		hero.moving = false
		
		if hero.ground and love.keyboard.isDown("lshift") then
			hero.sprint = hero.speed/6
			hero.anim["walk"]:setSpeed(0.08)
		else
			hero.anim["walk"]:setSpeed(nil)
			hero.sprint = 0
		end
		
		if not (hero.hanging) then
			if hero.sliding ~= "right" then
				if love.keyboard.isDown("left") then
					hero.moving = "left"
				   	hero:move(dt, 5*-hero.speed*dt-hero.sprint, 0)
					hero.facing = "left"
				end
			end
			if hero.sliding ~= "left" then
				if love.keyboard.isDown("right") then
					hero.moving = "right"
				  	hero:move(dt, 5*hero.speed*dt+hero.sprint, 0)
					hero.facing = "right"
				end
			end
			if love.keyboard.isDown("up") then
				--hero:move(0, -150)
			end	
		end
		if hero.hanging then
			if love.keyboard.isDown("down") then
				hero.hanging = false
				hero.climbing = false
				
				--print("what")
			elseif (not hero.climbing) and love.keyboard.isDown("up") then
				hero.climbing = true
				hero.anim["hangup"]:play()
			end
			
		else
			if love.keyboard.isDown(" ") then
				if (not hero.jumping) and hero.ground == true then 
				--print("break")
					
					if hero:getTile(0,-2) then
							--print("lol")
					else
					
						hero.jumping = true
						hero.jump = hero.jumpSpeed
					
						--Don't jump into a ceiling.
						for i=0, round(hero.jumpSpeed/140+1) do
							if hero:getTile(0,-(i+1)) then
								hero.jump = 100+(i*42)
								break
							end
						end
						--hero.jump = 160
						hero:move(dt, 0, -hero.jump)
					end
				end
			end
		end
		if hero.hanging then
			--print("hang")
			if hero.ground then
				--if not hero.anim["hangup"].playing then
					hero.hanging = false
					hero.climbing = false
					hero.anim["hang"]:stop()
					--print("lol")
				--end
			elseif hero.climbing then
				if hero.facing == "left" then
					hero:move(dt, -90, -130)
				end
				if hero.facing == "right" then
					hero:move(dt, 90, -130)
				end
			else
				hero:move(dt, 0, -100)
			end
			
		elseif hero.jumping then
			hero:move(dt, 0, -hero.jump)
			hero.jump = hero.jump - 5
			--print(hero.jump)
			if hero.ground == true then
				if hero.jumping and hero.jump > gravity then
					hero.jump = gravity
				end
				if hero.jump <= 0 then
					hero.jumping = false
				end
			end
		end
		if hero.sliding then
			hero.facing = hero.sliding
		end
	end
	heroes.__collider:update(dt)
	animation.play(dt)
end

--Draw heroes.
heroes.draw = function()
	for i,hero in pairs(heroes.__heroesInGame) do
		local x , y = hero.x,hero.y
		--print(x,y)
		
		if hero.anim then
			if hero.climbing then
				hero.anim["hangup"].x = (x *128) +(128/2)
				hero.anim["hangup"].y = y *128 - 30
				if hero.facing == "left" then
					animation.__flip = true
				else
					animation.__flip = false
					
				end
				hero.anim["hangup"]:draw(lg)
			elseif hero.hanging then
				hero.anim["hang"].x = (x *128) +(128/2)
				hero.anim["hang"].y = y *128 - 30
				if hero.facing == "left" then
					animation.__flip = true
				else
					animation.__flip = false
					
				end
				hero.anim["hang"]:draw(lg)
			elseif hero.jumping then
				hero.anim["jump"].x = (x *128) +(128/2)
				hero.anim["jump"].y = y *128 - 30 
				if hero.facing == "left" then
					animation.__flip = true
				else
					animation.__flip = false
				end
				hero.anim["jump"]:draw(lg)
			elseif hero.moving then
				--print(y)
				hero.anim["walk"].x = (x *128) +(128/2)
				hero.anim["walk"].y = y *128 - 30
				if hero.facing == "left" then
					animation.__flip = true
					hero.anim["walk"]:draw(lg)
				else
					animation.__flip = false
					hero.anim["walk"]:draw(lg)
				end
			else
				if hero.sliding == "right" then 
					animation.__flip = false
					r = -math.pi/2/2 
					hero.anim["stand"].x = (x *128) +(128/2)-20
					hero.anim["stand"].y = y *128  - 30 + 80
				elseif hero.sliding == "left" then 
					animation.__flip = true
					r = -math.pi/2/2 
					hero.anim["stand"].x = (x *128) +(128/2) +20
					hero.anim["stand"].y = y *128  - 30 + 100
				else 
					r = 0
					hero.anim["stand"].x = (x *128) +(128/2)
					hero.anim["stand"].y = y *128  - 30
				end 
				hero.anim["stand"]:draw(lg, 0, 0, r)
				animation.draw()
			end
			--hero.anim["stand"].x = (2 *128) +(128/2)
			--hero.anim["stand"].y = 4 *128  - 30 
			--hero.anim["stand"]:draw(lg)
			
		else
			love.graphics.draw(hero.image,x,y,0,1,1,hero.image:getWidth()/2,hero.image:getHeight()/2)
		end
	end
	animation.draw()
end


--Setup heroes.
heroes.init = function(map)
	
	
    -- load HardonCollider, set callback to on_collide and size of 100
    heroes.__collider = HC(100, on_collide)
    
        -- find all the tiles that we can collide with
    allSolidTiles = findSolidTiles(map,collider)

	-- Shortcut to the layer
	local layer = map("Player")
	
	local player
	
	for i, obj in pairs( map("Player").objects ) do
    		print(obj.x, obj.y)
			player = heroes.spawnHero("uni", obj.x/128, obj.y/128)
	end


	-- Convert the object layer into a custom layer. Pass the conversion function.
	layer:toCustomLayer()


	-- Custom layer draw function
	function layer:draw()
		heroes.draw()
	end
	
	return player
end

--Setup collisions for map.
function findSolidTiles(map)

    local collidable_tiles = {}

    for x, y, tile in map("ground"):iterate() do
    
            if tile then
                local ctile = heroes.__collider:addRectangle(x,y,1,1)
                ctile.type = "tile"
                heroes.__collider:addToGroup("tiles", ctile)
                heroes.__collider:setPassive(ctile)
                table.insert(collidable_tiles, ctile)
            end

    end

    return collidable_tiles
end


function on_collide(dt, shape_a, shape_b, mtv_x, mtv_y)

    collideHeroWithTile(dt, shape_a, shape_b, mtv_x, mtv_y)

end

function collideHeroWithTile(dt, shape_a, shape_b, mtv_x, mtv_y)

    -- sort out which one our hero shape is
    local hero_shape, tileshape
    if shape_a.type == "player" and shape_b.type == "tile" then
        hero_shape = shape_a
    elseif shape_b.type == "player" and shape_a.type == "tile" then
        hero_shape = shape_b
    else
        -- none of the two shapes is a tile, return to upper function
        return
    end

    -- why not in one function call? because we will need to differentiate between the axis later
    hero_shape:move(mtv_x, 0)
    hero_shape:move(0, mtv_y)

end

return heroes
