local heroes = {}
heroes.__heroes = {}
heroes.__heroesInGame = {}
heroes.__collider = nil
local HC = require "HardonCollider"


require ("anim")

heroes.Hero = {}
heroes.Hero.__index = heroes.hero

--Register new Hero.
heroes.newHero = function(id, table)
	local hero = {}
	hero.name = tostring(table.name)
	hero.image = table.image
	hero.anim = table.anim
    hero.speed = 400
	heroes.__heroes[id] = hero
end

--Spawn Hero in the game.
heroes.spawnHero = function(id, x, y)
	local hero = {}
	setmetatable(hero, heroes.Hero)
	hero.name = tostring(name)
	hero.image = heroes.__heroes[id].image
	hero.x = tonumber(x)
	hero.y = tonumber(y)
	hero.speed = heroes.__heroes[id].speed
	hero.body = love.physics.newBody(world, hero.x, hero.y,"dynamic")
	hero.shape = love.physics.newRectangleShape(hero.image:getWidth(),240)
	table.insert(heroes.__heroesInGame,hero)
	
	if heroes.__heroes[id].anim then 
		hero.anim = {}
		hero.anim["stand"] = animation.load("data/anim/uni_stand.anim", "data/")
		hero.anim["stand"]:run()
		hero.anim["walk"] = animation.load("data/anim/uni_walk.anim", "data/")
		hero.anim["walk"]:run()
		hero.anim["jump"] = animation.load("data/anim/uni_jump.anim", "data/")
		hero.anim["jump"]:run()
	end
	
	return hero
end

--Update heroes.
heroes.update = function(dt)
	for i,hero in pairs(heroes.__heroesInGame) do
		hero.body:move(0,600*dt)
		hero.moving = false
		hero.jumping = false
		 if love.keyboard.isDown("left") then
		    hero.body:move(-hero.speed*dt, -300*dt)
		    hero.moving = "left"
		end
		if love.keyboard.isDown("right") then
		    hero.body:move(hero.speed*dt, -300*dt)
		    hero.moving = "right"
		end
		if love.keyboard.isDown(" ") then
			hero.body:move(0, -hero.speed*dt)
			hero.jumping = "true"
		end	
	end
	world:update(dt)
	animation.play(dt)
end

--Draw heroes.
heroes.draw = function()
	for i,hero in pairs(heroes.__heroesInGame) do
		local x , y = hero.body:center()
		if hero.anim then
			if hero.jumping then
				hero.anim["jump"].x = x
				hero.anim["jump"].y = y - 30
				hero.anim["jump"]:draw(lg)
			elseif hero.moving then
				hero.anim["walk"].x = x
				hero.anim["walk"].y = y - 30
				if hero.moving == "left" then
					animation.__flip = true
					hero.anim["walk"]:draw(lg)
				else
					animation.__flip = false
					hero.anim["walk"]:draw(lg)
				end
			else
				hero.anim["stand"].x = x
				hero.anim["stand"].y = y - 30
				hero.anim["stand"]:draw(lg)
			end
		else
			love.graphics.draw(hero.image,x,y,0,1,1,hero.image:getWidth()/2,hero.image:getHeight()/2)
		end
	end
	animation.draw()
end


--Setup heroes.
heroes.init = function(map)
	
	
    -- load HardonCollider, set callback to on_collide and size of 100
    heroes.__collider = HC(200, on_collide)
    
        -- find all the tiles that we can collide with
    allSolidTiles = findSolidTiles(map,collider)

	-- Shortcut to the layer
	local layer = map("Player")

	-- Convert the object layer into a custom layer. Pass the conversion function.
	layer:toCustomLayer(function() end)

	-- Custom layer draw function
	function layer:draw()
		heroes.draw()
	end
end

--Setup collisions for map.
function findSolidTiles(map)

    local collidable_tiles = {}

    for x, y, tile in map("ground"):iterate() do
            if tile then
           		local tile = love.physics.newBody(world, (x-1)*128+128,(y-1)*128+128,"static"),
				local shape = love.physics.newRectangleShape(128,128),
            end

    end

    return collidable_tiles
end

return heroes
