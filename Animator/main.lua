function love.load()
		--Fullscreen function
		modes = love.graphics.getModes()
		table.sort(modes, function(a, b) return a.width*a.height < 	b.width*b.height end)   -- sort from smallest to largest
		table.getn(modes)
		windowWidth = modes[table.getn(modes)].width and 1366 and 800
		windowHeight = modes[table.getn(modes)].height and 768 and 600
		--love.graphics.setMode(1024,768, false, true, 0)
		love.graphics.setMode(windowWidth,windowHeight, false, true, 0) --set the window dimensions to 650 by 650 with no fullscreen, vsync on, and no antialiasing
		windowWidth, windowHeight = love.graphics.getMode( )
		
		require("api")
		
		--body = animation.load("parent", "uni.anim")
		
		if not body then
		
			--anim = animation.newAnimation("stand")
			anim = animation.load(arg[2] or "uni.anim", "data/")
			
			--[[anim:newEntities({ "body",
				{
					{"head"},
					{"right arm", 
						{"right hand"}
					},
					{"left arm", 
						{"left hand"}
					},
					{"right leg", 
						{"right foot"}
					},
					{"left leg", 
						{"left foot"}
					},
				}
			})]]
			
			anim:getEntity("body"):setImage("body.png")
			anim:getEntity("head"):setImage("head.png")
			anim:getEntity("right arm"):setImage("sleve.png")
			anim:getEntity("left arm"):setImage("sleve.png")
			anim:getEntity("right hand"):setImage("arm.png")
			anim:getEntity("left hand"):setImage("arm.png")
			anim:getEntity("right leg"):setImage("leg.png")
			anim:getEntity("left leg"):setImage("leg.png")
			anim:getEntity("right foot"):setImage("foot.png")
			anim:getEntity("left foot"):setImage("foot.png")
		end
		
		
		sprite = {}
		sprite.img = love.graphics.newImage("data/uni.png")
		sprite.img1 = love.graphics.newImage("data/uni-walk1.png")
		sprite.img2 = love.graphics.newImage("data/uni-walk2.png")
		sprite.img3 = love.graphics.newImage("data/uni-walk3.png")
		sprite.x = 0
		sprite.y = 0
		sprite.facing = "left"
		time = os.clock()
end

function love.keypressed(key)
	animation.keypressed(key)
end

function love.update(dt)
	animation.edit()
	animation.play(dt)
	local sprite = anim
	sprite.moving = false
	if love.keyboard.isDown("w") then
		sprite.y = sprite.y - 5
		sprite.moving = true
	elseif love.keyboard.isDown("s") then
		sprite.y = sprite.y + 5
		sprite.moving = true
	end
	if love.keyboard.isDown("a") then
		sprite.x = sprite.x - 5
		sprite.moving = true
		sprite.facing="left"
	elseif love.keyboard.isDown("d") then
		sprite.moving = true
		sprite.x = sprite.x + 5
		sprite.facing="right"
	end
end

function love.draw()
	local lg = love.graphics
	
	animation.render(lg)
end
